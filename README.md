# Odoo Quickstart

## Setup

1. Clone this repository

```sh
git clone git@gitlab.com:Accomodata/odoo-quickstart.git
```

2. Start the database container

```sh
docker-compose -f docker-compose-db.yml up -d
```

3. Start the Odoo container

```sh
docker-compose up
```

4. Open your browser and open Odoo at `localhost:8069`

```sh
xdg-open localhost:8069
```

5. Login

user: `admin`
password: `admin`

6. Odoo source code (optional)

If you would like to have the source code of Odoo locally, as a reference, this can
be done via

```sh
git submodule update --init
```
